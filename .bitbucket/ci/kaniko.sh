echo "{\"auths\":{\"https://index.docker.io/v1/\":{\"auth\":\"${DOCKERHUB_AUTH}\"}}}" > /kaniko/.docker/config.json

/kaniko/executor --context "$CONTEXT" --dockerfile "$DOCKERFILE_PATH" --destination "$IMAGE_REPOSITORY_URI:$IMAGE_TAG"

# bitbucket CI hangs if /kaniko/executor is the last command, explicitly exit with 'exit code' from executor
exit $?
