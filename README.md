# 🚨 All functionality moved to [bitbucket-kaniko](https://bitbucket.org/visioncraft-ai/bitbucket-kaniko/src/main/) repo 🚨#

Inspired by [npm-publish pipe](https://bitbucket.org/atlassian/npm-publish/src/master/).

This pipe is using [bitbucket-kaniko image](https://bitbucket.org/visioncraft-ai/bitbucket-kaniko/src/main/).

