FROM visioncraftai/bitbucket-kaniko:debug

COPY pipe /
RUN chmod +x /pipe.sh

COPY common.sh README.md /

ENTRYPOINT ["/pipe.sh"]
