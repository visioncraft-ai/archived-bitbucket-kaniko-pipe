#!/bin/sh
#
# kaniko pipe
#
# Required globals:
#   IMAGE_REPOSITORY_URI
#   
#
# Optional globals:
#   DEBUG
#   DOCKER_CONFIG_FILE
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   CONTEXT
#   DOCKERFILE_PATH
#   IMAGE_TAG
#   EXTRA_IMAGE_TAGS
#   EXECUTOR_EXTRA_ARGS
#

# common utilities from bitbucket
source "/common.sh"

info "Starting pipe execution..."

if [ "${DEBUG}" == "true" ]; then
  info "Enabling debug mode."
  set -x
fi

# docker config
_docker_config_set=false
if [ $_docker_config_set != true -a -n "$DOCKER_CONFIG_FILE" -a -f "$DOCKER_CONFIG_FILE" ]; then
  info "Using docker config file from ${DOCKER_CONFIG_FILE}"
  cp $DOCKER_CONFIG_FILE /kaniko/.docker/config.json
  _docker_config_set=true
fi

if [ $_docker_config_set != true -a -n "$AWS_ACCESS_KEY_ID" -a -n "$AWS_SECRET_ACCESS_KEY" ]; then
  info "AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY variables detected, setting ecr-login"
  echo "{\"credsStore\":\"ecr-login\"}" > /kaniko/.docker/config.json
  _docker_config_set=true
fi

# default context (needs to be absolute path)
if [ ! -n "$CONTEXT" ]; then
  CONTEXT=$BITBUCKET_CLONE_DIR
fi
# default dockerfile path
if [ ! -n "$DOCKERFILE_PATH" ]; then
  DOCKERFILE_PATH="${BITBUCKET_CLONE_DIR}/Dockerfile"
fi
# default tag
if [ ! -n "$IMAGE_TAG" ]; then
  DOCKERFILE_PATH="latest"
fi


# make destinations from tags
destinations="--destination $IMAGE_REPOSITORY_URI:$IMAGE_TAG"
set -- $EXTRA_IMAGE_TAGS
while [ -n "$1" ]; do 
  destinations="${destinations} --destination $IMAGE_REPOSITORY_URI:$1"
  shift
done

# make and set additional arguments for kaniko executor command
kaniko_args="${destinations} ${EXECUTOR_EXTRA_ARGS}"
set -- $kaniko_args

run /kaniko/executor \
--context "$CONTEXT" \
--dockerfile "$DOCKERFILE_PATH" \
--image-name-tag-with-digest-file "/dev/termination-log" \
--label bitbucket=true \
--label kaniko=true \
--log-timestamp=true \
--push-retry 3 \
$@

if [ "${status}" -eq 0 ]; then
  success "success"
else
  fail "fail"
fi
